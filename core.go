package partner_core

import (
	"encoding/json"
)

type Partner struct {
	Name       string
	Address    string
	WebAddress string
}

func (p *Partner) Serializer() (string, error) {
	b, err := json.Marshal(p)
	if err != nil {
		return "", err
	}

	return string(b), err
}

func (p *Partner) ToCsv() []string {
	csvFormat := []string{p.Name, p.Address, p.WebAddress}
	return csvFormat
}
