package partner_core

/**
 * :=  created by:  Shuza
 * :=  create date:  21-Feb-2019
 * :=  (C) CopyRight Shuza
 * :=  www.shuza.ninja
 * :=  shuza.sa@gmail.com
 * :=  Fun  :  Coffee  :  Code
 **/

type PartnerSource interface {
	Init() error
	Fetch() ([]Partner, bool)
}
